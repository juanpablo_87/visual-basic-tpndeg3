﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtpparcial = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtptot3 = New System.Windows.Forms.TextBox()
        Me.txtcant3 = New System.Windows.Forms.TextBox()
        Me.txtpunit3 = New System.Windows.Forms.TextBox()
        Me.txtdescripcion3 = New System.Windows.Forms.TextBox()
        Me.txtcodigo3 = New System.Windows.Forms.TextBox()
        Me.txtptot2 = New System.Windows.Forms.TextBox()
        Me.txtcant2 = New System.Windows.Forms.TextBox()
        Me.txtpunit2 = New System.Windows.Forms.TextBox()
        Me.txtdescripcion2 = New System.Windows.Forms.TextBox()
        Me.txtcodigo2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtptotal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtcant = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtpunit = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtdescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtentiva = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtentdesc = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txttotal = New System.Windows.Forms.TextBox()
        Me.txtiva = New System.Windows.Forms.TextBox()
        Me.txtdescuento = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtpparcial)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.txtptot3)
        Me.GroupBox1.Controls.Add(Me.txtcant3)
        Me.GroupBox1.Controls.Add(Me.txtpunit3)
        Me.GroupBox1.Controls.Add(Me.txtdescripcion3)
        Me.GroupBox1.Controls.Add(Me.txtcodigo3)
        Me.GroupBox1.Controls.Add(Me.txtptot2)
        Me.GroupBox1.Controls.Add(Me.txtcant2)
        Me.GroupBox1.Controls.Add(Me.txtpunit2)
        Me.GroupBox1.Controls.Add(Me.txtdescripcion2)
        Me.GroupBox1.Controls.Add(Me.txtcodigo2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtptotal)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtcant)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtpunit)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtdescripcion)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtcodigo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(337, 211)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtpparcial
        '
        Me.txtpparcial.BackColor = System.Drawing.Color.White
        Me.txtpparcial.Location = New System.Drawing.Point(246, 167)
        Me.txtpparcial.Name = "txtpparcial"
        Me.txtpparcial.ReadOnly = True
        Me.txtpparcial.Size = New System.Drawing.Size(73, 20)
        Me.txtpparcial.TabIndex = 27
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(191, 171)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "P.Parcial"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(128, 167)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(56, 23)
        Me.Button1.TabIndex = 24
        Me.Button1.Text = "Calcular"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtptot3
        '
        Me.txtptot3.BackColor = System.Drawing.Color.White
        Me.txtptot3.Location = New System.Drawing.Point(278, 117)
        Me.txtptot3.Name = "txtptot3"
        Me.txtptot3.ReadOnly = True
        Me.txtptot3.Size = New System.Drawing.Size(53, 20)
        Me.txtptot3.TabIndex = 23
        '
        'txtcant3
        '
        Me.txtcant3.Location = New System.Drawing.Point(214, 117)
        Me.txtcant3.Name = "txtcant3"
        Me.txtcant3.Size = New System.Drawing.Size(53, 20)
        Me.txtcant3.TabIndex = 22
        '
        'txtpunit3
        '
        Me.txtpunit3.Location = New System.Drawing.Point(146, 117)
        Me.txtpunit3.Name = "txtpunit3"
        Me.txtpunit3.Size = New System.Drawing.Size(53, 20)
        Me.txtpunit3.TabIndex = 21
        '
        'txtdescripcion3
        '
        Me.txtdescripcion3.Location = New System.Drawing.Point(76, 117)
        Me.txtdescripcion3.Name = "txtdescripcion3"
        Me.txtdescripcion3.Size = New System.Drawing.Size(53, 20)
        Me.txtdescripcion3.TabIndex = 20
        '
        'txtcodigo3
        '
        Me.txtcodigo3.Location = New System.Drawing.Point(6, 117)
        Me.txtcodigo3.Name = "txtcodigo3"
        Me.txtcodigo3.Size = New System.Drawing.Size(53, 20)
        Me.txtcodigo3.TabIndex = 19
        '
        'txtptot2
        '
        Me.txtptot2.BackColor = System.Drawing.Color.White
        Me.txtptot2.Location = New System.Drawing.Point(278, 85)
        Me.txtptot2.Name = "txtptot2"
        Me.txtptot2.ReadOnly = True
        Me.txtptot2.Size = New System.Drawing.Size(53, 20)
        Me.txtptot2.TabIndex = 18
        '
        'txtcant2
        '
        Me.txtcant2.Location = New System.Drawing.Point(214, 85)
        Me.txtcant2.Name = "txtcant2"
        Me.txtcant2.Size = New System.Drawing.Size(53, 20)
        Me.txtcant2.TabIndex = 17
        '
        'txtpunit2
        '
        Me.txtpunit2.Location = New System.Drawing.Point(146, 85)
        Me.txtpunit2.Name = "txtpunit2"
        Me.txtpunit2.Size = New System.Drawing.Size(53, 20)
        Me.txtpunit2.TabIndex = 16
        '
        'txtdescripcion2
        '
        Me.txtdescripcion2.Location = New System.Drawing.Point(76, 85)
        Me.txtdescripcion2.Name = "txtdescripcion2"
        Me.txtdescripcion2.Size = New System.Drawing.Size(53, 20)
        Me.txtdescripcion2.TabIndex = 15
        '
        'txtcodigo2
        '
        Me.txtcodigo2.Location = New System.Drawing.Point(6, 85)
        Me.txtcodigo2.Name = "txtcodigo2"
        Me.txtcodigo2.Size = New System.Drawing.Size(53, 20)
        Me.txtcodigo2.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(90, 159)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(0, 13)
        Me.Label6.TabIndex = 11
        '
        'txtptotal
        '
        Me.txtptotal.BackColor = System.Drawing.Color.White
        Me.txtptotal.Location = New System.Drawing.Point(278, 50)
        Me.txtptotal.Name = "txtptotal"
        Me.txtptotal.ReadOnly = True
        Me.txtptotal.Size = New System.Drawing.Size(53, 20)
        Me.txtptotal.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(278, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "P.Total"
        '
        'txtcant
        '
        Me.txtcant.Location = New System.Drawing.Point(214, 50)
        Me.txtcant.Name = "txtcant"
        Me.txtcant.Size = New System.Drawing.Size(53, 20)
        Me.txtcant.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(211, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Cant"
        '
        'txtpunit
        '
        Me.txtpunit.Location = New System.Drawing.Point(146, 50)
        Me.txtpunit.Name = "txtpunit"
        Me.txtpunit.Size = New System.Drawing.Size(53, 20)
        Me.txtpunit.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(148, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "P.Unit"
        '
        'txtdescripcion
        '
        Me.txtdescripcion.Location = New System.Drawing.Point(76, 50)
        Me.txtdescripcion.Name = "txtdescripcion"
        Me.txtdescripcion.Size = New System.Drawing.Size(53, 20)
        Me.txtdescripcion.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(73, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Descripcion"
        '
        'txtcodigo
        '
        Me.txtcodigo.Location = New System.Drawing.Point(6, 50)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.Size = New System.Drawing.Size(53, 20)
        Me.txtcodigo.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboBox1)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.txtentiva)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtentdesc)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txttotal)
        Me.GroupBox2.Controls.Add(Me.txtiva)
        Me.GroupBox2.Controls.Add(Me.txtdescuento)
        Me.GroupBox2.Location = New System.Drawing.Point(-4, 214)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(343, 153)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(71, 115)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(49, 23)
        Me.Button3.TabIndex = 28
        Me.Button3.Text = "Nuevo"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(134, 73)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(24, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "IVA"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(16, 115)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(49, 23)
        Me.Button2.TabIndex = 25
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'txtentiva
        '
        Me.txtentiva.Location = New System.Drawing.Point(175, 66)
        Me.txtentiva.Name = "txtentiva"
        Me.txtentiva.Size = New System.Drawing.Size(31, 20)
        Me.txtentiva.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(110, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Descuento"
        '
        'txtentdesc
        '
        Me.txtentdesc.Location = New System.Drawing.Point(175, 28)
        Me.txtentdesc.Name = "txtentdesc"
        Me.txtentdesc.Size = New System.Drawing.Size(31, 20)
        Me.txtentdesc.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(160, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Total $"
        '
        'txttotal
        '
        Me.txttotal.BackColor = System.Drawing.Color.White
        Me.txttotal.Location = New System.Drawing.Point(225, 108)
        Me.txttotal.Name = "txttotal"
        Me.txttotal.ReadOnly = True
        Me.txttotal.Size = New System.Drawing.Size(100, 20)
        Me.txttotal.TabIndex = 13
        '
        'txtiva
        '
        Me.txtiva.BackColor = System.Drawing.Color.White
        Me.txtiva.Location = New System.Drawing.Point(225, 66)
        Me.txtiva.Name = "txtiva"
        Me.txtiva.ReadOnly = True
        Me.txtiva.Size = New System.Drawing.Size(100, 20)
        Me.txtiva.TabIndex = 12
        '
        'txtdescuento
        '
        Me.txtdescuento.BackColor = System.Drawing.Color.White
        Me.txtdescuento.Location = New System.Drawing.Point(224, 28)
        Me.txtdescuento.Name = "txtdescuento"
        Me.txtdescuento.ReadOnly = True
        Me.txtdescuento.Size = New System.Drawing.Size(100, 20)
        Me.txtdescuento.TabIndex = 11
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(12, 19)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 28
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(345, 373)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Factura de Venta"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtptotal As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtcant As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtpunit As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtdescripcion As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtcodigo As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txttotal As TextBox
    Friend WithEvents txtiva As TextBox
    Friend WithEvents txtdescuento As TextBox
    Friend WithEvents txtptot3 As TextBox
    Friend WithEvents txtcant3 As TextBox
    Friend WithEvents txtpunit3 As TextBox
    Friend WithEvents txtdescripcion3 As TextBox
    Friend WithEvents txtcodigo3 As TextBox
    Friend WithEvents txtptot2 As TextBox
    Friend WithEvents txtcant2 As TextBox
    Friend WithEvents txtpunit2 As TextBox
    Friend WithEvents txtdescripcion2 As TextBox
    Friend WithEvents txtcodigo2 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtentiva As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtentdesc As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents txtpparcial As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents ComboBox1 As ComboBox
End Class
